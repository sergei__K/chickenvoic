package ChickenVoic;

public class ChickenVoice {

    static EggVoice mAnotherOpinion;    //Побочный поток

    public static void main(String[] args) {
        mAnotherOpinion = new EggVoice();    //Создание потока
        System.out.println("Спор начат...");
        mAnotherOpinion.start();         //Запуск потока
        mAnotherOpinion.setPriority(10);

        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(1000);        //Приостанавливает поток на 1 секунду
            } catch (InterruptedException e) {
            }

            System.out.println("курица!");
        }
    }
}